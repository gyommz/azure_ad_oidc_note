## Some usefull docs to read :

- [How Azure work ?](https://learn.microsoft.com/en-us/azure/cloud-adoption-framework/get-started/what-is-azure)

- [OAuth 2.0 et OpenID Connect (OIDC)](https://learn.microsoft.com/fr-fr/entra/identity-platform/v2-protocols)

- [Official Microsoft Entra Doc](https://learn.microsoft.com/en-us/entra/)

- [How to setup the client credentials flow](https://learn.microsoft.com/en-us/azure/active-directory-b2c/client-credentials-grant-flow?pivots=b2c-user-flow)

- [RFC](https://datatracker.ietf.org/doc/html/rfc6749)

- [Why we should go for auth flow](https://learn.microsoft.com/en-us/entra/identity-platform/v2-oauth2-implicit-grant-flow?WT.mc_id=Portal-Microsoft_AAD_RegisteredApps#prefer-the-auth-code-flow)

- [The lib we use for decode jwt token in the xuc](https://jwt-scala.github.io/jwt-scala/jwt-play-json.html)

- [x5c](https://mojoauth.com/glossary/jwt-x.509-certificate-chain/#:~:text=The%20%22x5c%22%20claim%20is%20an,authority%20to%20the%20signing%20certificate.)

<details>
<summary>
Azure AD Configuration
</summary>

- User
  - [Create a user](https://learn.microsoft.com/en-us/power-platform/admin/create-users)

  - [Create a group](https://learn.microsoft.com/en-us/entra/fundamentals/groups-view-azure-portal#create-a-new-group)

  - [Add user to group](https://learn.microsoft.com/en-us/entra/fundamentals/groups-view-azure-portal#add-a-group-member)

- App Registrations
  - [Create Application](https://learn.microsoft.com/en-us/entra/identity-platform/quickstart-register-app#register-an-application)
  here you can see the endpoints => ![](images/endpoints.png)

  - Microsoft is officially asking users NOT to use v1.0 for new project on their website see :

    - https://learn.microsoft.com/en-us/entra/identity-platform/v2-overview
    - https://learn.microsoft.com/en-us/entra/identity-platform/v2-overview

  - Use V2 tokens for that simply change the accessTokenAcceptedVersion to 2 ![](images/manifest.png)

  - Create a clien secret via the Certificates & secrets section & note the value of the secret this will be show only ⚠️**ONCE**⚠️ so make sure to save it ![](images/secret.png)

  - [Add api permissions](https://learn.microsoft.com/en-us/entra/identity-platform/quickstart-configure-app-access-web-apis) ![](images/permissions.png) adjust for the informations that you need

  - Add optionnal claims for the access_token
    - aud // necessary
    - preffered_username, given_name, upn, or any necessary claims you need to match you user with xivo
  ![](images/optionnal_claim.png)

  ```

</details>

<details>

<summary>
Imsomnia
</summary>
- Token request with insomnia
  - use the raw to import collection and fill env variable

  - do the first request "authorization_code", from there u will have to login change password and grant the permission ![](images/grant_permissions.png)

  - add the new password in env variable then make the password request and tada you have your token ![](images/token.png)
</details>

<details>
<summary>
XIVOCC Configuration
</summary>
on xivocc put these variable in custom.env

  ```
  ENABLE_OIDC=true
  OIDC_SERVER_URL="https://login.microsoftonline.com/TENANT_ID/oauth2/v2.0"
  OIDC_CLIENT_ID="CLIENT_ID"
  OIDC_ADDITIONAL_SERVERS_URL="https://sts.windows.net/TENANT_ID/"
  OIDC_ADDITIONAL_CLIENT_IDS="https://sts.windows.net/TENANT_ID/"
  OIDC_AUDIENCE="" // you can find it in the manifest with the following variable : "resourceAppId"
  OIDC_USERNAEM_FIELD="" // the token field you want to use to match you cti login (ex: upn, name => will be used to link the user in db)
  ```

</details>

<details>
<summary>
TODO
</summary>

Some step of the todo are already done in the poc so i wont list them here you can look at the current state section

- Choose the correct authentication flows we want to make you can find a list of scenario and supported auth flow [here](https://learn.microsoft.com/en-us/entra/identity-platform/authentication-flows-app-scenarios#scenarios-and-supported-authentication-flows) => going for the implicit flow for now.

Here the flows with our wanted implementation : 
- Authorization Flow
![](images/Authorization_flow.drawio.png)

- Implicit Flow
![](images/implicit_flow.drawio.png)

- Choose a optionnal claim field to retrieve the user ("preferred_name" vs "unique_name" vs "upn") => this is configurable with the custom.env

- Since the token is through the fragment encoding, we modify the way we retrieve token from the url => this will be a second step if possible
  - In the context of OAuth 2.0, the recommended and more secure way to transmit access tokens is through the fragment encoding (response_mode=fragment), which is the default for the OAuth 2.0 token response type. Transmitting access tokens in the query string can expose sensitive information in server logs and may pose security risks.

- Get all the config necessary from the well-know/openid-configuration endpoint

- Need to modify all keycloak part to be closer to standard (use a more strict way of verifying the signature of the token, like microsoft) ?

- For keycloak we use implicit flow, do we keep the implicit flow for keycloak ? How to handle the difference between Keycloak and Azure?
</details>

<details>
<summary>
Current state
</summary>

For both project you need an application-dev.conf with these variables :

- xuc:

```
include "application.conf"

XIVO_HOST =
USM_EVENT_AMQP_SECRET = 1234
PLAY_AUTH_TOKEN =
XIVO_AMI_SECRET =
ENABLE_OIDC = true
OIDC_SERVER_URL = "https://login.microsoftonline.com/TENANT_ID/oauth2/v2.0/authorize?"
OIDC_CLIENT_ID = "CLIENT_ID"
OIDC_ADDITIONAL_SERVERS_URL = "https://sts.windows.net/TENANT_ID/v2.0"
oidc.usernameField = "unique_name"
oidc.azurePubKeyUrl =
```

- xucmgt

```
include "application.conf"

XUC_HOST = localhost
XUC_PORT = 8090

OIDC_LOGOUT_ENABLE = true
OIDC_SERVER_URL = "https://login.microsoftonline.com/TENANT_ID/oauth2/v2.0/authorize?"
OIDC_CLIENT_ID = "CLIENT_ID"
```

You can find the current state on the poc with these MR :
- [xuc](https://gitlab.com/xivo.solutions/xucserver/-/merge_requests/999/diffs)
- [xucmgt](https://gitlab.com/xivo.solutions/xucmgt/-/merge_requests/1095/diffs)
</details>
