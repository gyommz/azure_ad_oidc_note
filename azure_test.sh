#!/bin/bash

export TENANT_ID=
export CLIENT_ID=
export CLIENT_SECRET=

export USERNAME=
export PASSWORD=

export AUTH_ENDPOINT=https://login.microsoftonline.com/574800fa-6081-4b51-a885-6fb509ee8647/oauth2/v2.0/authorize
export TOKEN_ENDPOINT=https://login.microsoftonline.com/574800fa-6081-4b51-a885-6fb509ee8647/oauth2/v2.0/token

getClientToken() {
    curl --request GET \
    --url $TOKEN_ENDPOINT \
    --header 'Content-Type: application/x-www-form-urlencoded' \
    --data grant_type=client_credentials \
    --data client_id=$CLIENT_ID \
    --data client_secret=$CLIENT_SECRET \
    --data "scope=$CLIENT_ID/.default"
}

getTokenForUser() {
    curl --request GET \
    --url $TOKEN_ENDPOINT \
    --header 'Content-Type: application/x-www-form-urlencoded' \
    --data grant_type=password \
    --data client_id=$CLIENT_ID \
    --data client_secret=$CLIENT_SECRET \
    --data username=$USERNAME \
    --data password=$PASSWORD \
    --data 'scope=openid profile email offline_access'
}


